# Front-end Dev - Test API

These are the instructions for the Front-End Dveloper technical "test" and the API to use with it.

## Getting Started

This repo only contains the API server that you can run your work against. It is just a simple fastify API that provides the data that is required.

## The Task

* To get this server running all you need to do is run "node server", I do recommend using a newer LTS edition of Node.
* From there, we would like you to create an app that takes this data, displays it in a meaningful manner that matches our brand guidelines and is sortable via filters.
* The filters we'd like are "alphabetical", "release year" and "Rotten Tomatoes score". Descending and ascending.
* Then we would like it if when you click on one of the titles it would open a modal or take you to a different page with a bit more detail.
* I understand this is a fairly large task but complete as much as possible and try and keep your code clean and concise. This is meant to be less of a test and just a way to see how you work and what your decision-making is like!
* Any extras you'd like to add would be great!

# The Final thing

If you could create a new repo from this one and host it either on Github, Gitlab or Bitbucket that'd be great.

## Authors

* **Luke Todd** - [lukeajtodd](https://github.com/lukeajtodd)
